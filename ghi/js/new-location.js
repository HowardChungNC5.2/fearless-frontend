// create an event listened for the contentloaded event
document.addEventListener("DOMContentLoaded", async function () {
  const url = "http://localhost:8000/api/states/";
  const response = await fetch(url);
  if (response.ok) {
    const responseData = await response.json();
    const element = document.querySelector("#state");
    // load options into the select element
    for (let state of responseData.states) {
      const option = document.createElement("option");
      option.text = state.name;
      option.value = state.abbreviation;
      element.add(option);
    }
  }
  // add an event listener for the submit event from the 'create-location-form' form.
  const form = document.querySelector("#create-location-form");
  form.addEventListener("submit", async (event) => {
    event.preventDefault();
    // do a POST request to the API
    const formData = new FormData(form);
    const json = JSON.stringify(Object.fromEntries(formData));
    const locationUrl = "http://localhost:8000/api/locations/";
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      form.reset();
      const newLocation = await response.json();
    }
  });
});

// GETTING 400 BAD REQUEST MESSAGE
// window.addEventListener("DOMContentLoaded", async () => {
//   const url = "http://localhost:8000/api/states/";
//   const response = await fetch(url);

//   if (response.ok) {
//     const data = await response.json();
//     // console.log("data:", data);

//     // Get the select tag element by its id 'state'
//     const selectTag = document.getElementById("state");
//     // For each state in the states property of the data
//     for (let state of data.states) {
//       // Create an 'option' element
//       // Set the '.value' property of the option element to the
//       // state's abbreviation
//       const option = document.createElement("option");
//       // Set the '.innerHTML' property of the option element to
//       // the state's name
//       option.innerHTML = state.name;
//       // Append the option element as a child of the select tag
//       selectTag.appendChild(option);
//     }
//   }
//   // add event listener to submit event from 'create-location-form' element
//   const formTag = document.getElementById("create-location-form");
//   formTag.addEventListener("submit", async (event) => {
//     event.preventDefault();
//     // console.log("need to submit the form data");
//     // convert html form data to json
//     const formData = new FormData(formTag);
//     const json = JSON.stringify(Object.fromEntries(formData));
//     console.log("json:", json);
//     const locationUrl = "http://localhost:8000/api/locations/";
//     const fetchConfig = {
//       method: "post",
//       body: json,
//       headers: {
//         "Content-Type": "application/json",
//       },
//     };
//     console.log("#################");
//     const response = await fetch(locationUrl, fetchConfig);
//     console.log("response:", response);
//     if (response.ok) {
//       formTag.reset();
//       const newLocation = await response.json();
//       console.log(newLocation);
//     }
//   });
// });
